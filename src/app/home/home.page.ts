import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  resultInput: string = '';

  constructor() { }

  setInput(number: string) {
    this.resultInput += number;
  }

  setResult() {
    this.resultInput = eval(this.resultInput);
  }

  clearInput(){
    this.resultInput = '';
  }

  clearLastInput(){    
    this.resultInput = this.resultInput.slice(0, -1);
  }
}
